package org.myorg;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
//import java.util.Map.Entry;
import java.util.Map.*;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.nio.ByteBuffer;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;
import java.io.IOException;
import java.util.regex.Pattern;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.StringUtils;

import org.apache.log4j.Logger;

public class InvertedIndex extends Configured implements Tool {

	private static final Logger LOG = Logger.getLogger(InvertedIndex.class);

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new InvertedIndex(), args);
		System.exit(res);
	}

	public int run(String[] args) throws Exception {
		Job job = Job.getInstance(getConf(), "invertedindex");
		for (int i = 0; i < args.length; i += 1) {
			if ("-skip".equals(args[i])) {
				job.getConfiguration().setBoolean("invertedindex.skip.patterns", true);
				i += 1;
				job.addCacheFile(new Path(args[i]).toUri());
				// this demonstrates logging
				LOG.info("Added file to the distributed cache: " + args[i]);
			}
		}

		job.setJarByClass(this.getClass());
		Path myout = new Path("/user/cloudera/invertedindex/");

		// Use TextInputFormat, the default unless job.setInputFormatClass is used
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		job.setMapperClass(Map.class);
		job.setCombinerClass(Combine.class);
		job.setReducerClass(Reduce.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		return job.waitForCompletion(true) ? 0 : 1;
		

	}

	public static class Map extends Mapper<LongWritable, Text, Text, Text> {
		private final static IntWritable one = new IntWritable(1);
		private Text word = new Text();
		private boolean caseSensitive = false;
		private long numRecords = 0;
		private String input;
		private Set<String> patternsToSkip = new HashSet<String>();
		private static final Pattern WORD_BOUNDARY = Pattern.compile("\\s*\\b\\s*");
		private static final Pattern CSV_BOUNDARY = Pattern.compile(",");
				
		private static final Pattern REGEX = Pattern.compile("[a-zA-Z]+(?:'[a-zA-Z]+)?");

		protected void setup(Mapper.Context context)
			throws IOException,
			       InterruptedException {

				       if (context.getInputSplit() instanceof FileSplit) {
					       this.input = ((FileSplit) context.getInputSplit()).getPath().toString();
				       } else {
					       this.input = context.getInputSplit().toString();
				       }	
				       Configuration config = context.getConfiguration();
				       this.caseSensitive = config.getBoolean("invertedindex.case.sensitive", false);
				       if (config.getBoolean("invertedindex.skip.patterns", false)) {
					       URI[] localPaths = context.getCacheFiles();
					       parseSkipFile(localPaths[0]);
				       }
			       }

		private void parseSkipFile(URI patternsURI) {
			LOG.info("Added file to the distributed cache: " + patternsURI);
			try {
				BufferedReader fis = new BufferedReader(new FileReader(new File(patternsURI.getPath()).getName()));
				String line;
				while ((line = fis.readLine()) != null) {
					
					for (String word : CSV_BOUNDARY.split(line)) {
						if (word.isEmpty()) {
							continue;
						} 
						patternsToSkip.add(word);
					} 
				}
			} catch (IOException ioe) {
				System.err.println("Caught exception while parsing the cached file '"
						+ patternsURI + "' : " + StringUtils.stringifyException(ioe));
			}
		}

		public void map(LongWritable offset, Text lineText, Context context)
			throws IOException, InterruptedException {
				String line = lineText.toString();
				String filename = ((FileSplit) context.getInputSplit()).getPath().getName();
				if (!caseSensitive) {
					line = line.toLowerCase();
				}
				Text currentWord = new Text();
				for (String word : WORD_BOUNDARY.split(line)) {
					
					if (word.isEmpty() || patternsToSkip.contains(word)) {
						continue;
					} else if (word.matches("[a-zA-Z]+(?:'[a-zA-Z]+)?")){
										
						currentWord = new Text(word);
						context.write(currentWord,new Text(filename));
					}
				}         
			}
	}

	
	public static class Combine extends Reducer<Text, Text, Text, Text> {
			
			
			//@Override			
			public void reduce(Text word, Iterable<Text> documents, Context context)
			throws IOException, InterruptedException {
			
				ArrayList<String> documentnames = new ArrayList<String>();
				ArrayList<Integer> instances = new ArrayList<Integer>();

				String docs = "";
				boolean firstentry = true;
				int i, currval;
				
				for (Text doc : documents)
				{
					
					currval = 0;
					
					if (documentnames.contains(doc.toString()))
					{
						currval = instances.get(documentnames.indexOf(doc.toString()));
						instances.set(documentnames.indexOf(doc.toString()), currval+1);
					}
					else
					{
						documentnames.add(doc.toString());
						instances.add(1);
					}
				}
				firstentry = true;

				for ( i = 0; i < documentnames.size(); i++)
				{
					
					if (firstentry)
					{
						firstentry = false;
						docs += documentnames.get(i) + " #"+instances.get(i);
					}
					else
					{
						docs += ", " + documentnames.get(i) + " #"+instances.get(i);
					}
				}
				context.write(word, new Text(docs));
			}
	}

	public static class Reduce extends Reducer<Text, Text, IntWritable, Text> {
			
			int wordscounter = 1;

			@Override			
			public void reduce(Text word, Iterable<Text> documents, Context context)
			throws IOException, InterruptedException {
			
				ArrayList<String> documentnames = new ArrayList<String>();
				
				String docs = "";
				boolean firstentry = true;
				int i;
				
				for (Text doc : documents)
				{
					documentnames.add(doc.toString());
				}
				firstentry = true;

				for ( i = 0; i < documentnames.size(); i++)
				{

					if (firstentry)
					{
						firstentry = false;
						docs += documentnames.get(i);
					}
					else
					{
						docs += ", " + documentnames.get(i);
					}
				}
				context.write(new IntWritable(wordscounter), new Text(word+"\t"+docs));
				wordscounter++;
			}
	}

}
