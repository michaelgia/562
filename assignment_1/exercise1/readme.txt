hadoop fs -mkdir /user/cloudera/wordcount /user/cloudera/wordcount/input

hadoop fs -put input/* /user/cloudera/wordcount/input

hadoop fs -put src/stop_words.text /user/cloudera/wordcount/

javac -cp /usr/lib/hadoop/*:/usr/lib/hadoop-mapreduce/* src/WordCount.java -d build -Xlint

jar -cvf wordcount.jar -C build/ .

hadoop jar wordcount.jar org.myorg.WordCount /user/cloudera/wordcount/input /user/cloudera/wordcount/output

or

hadoop jar wordcount.jar org.myorg.WordCount /user/cloudera/wordcount/input /user/cloudera/wordcount/output -skip /user/cloudera/wordcount/stop_words.text

hadoop fs -cat /user/cloudera/wordcount/output/*

export first 10:
	hadoop fs -cat /user/cloudera/wordcount/output/* | awk '{print $2}' | head -10 > first10.txt
	
	or

	hadoop fs -cat /user/cloudera/wordcount/output/* | awk '{print $2"\t"$1}' | head -10 > first10.txt

export csv file:
	
	hadoop fs -cat /user/cloudera/wordcount/output/* | awk '{print $2}' | sort | tr '\n' ',' | sed '$ \
	s/.$//' > stopwords.csv
	
	hadoop fs -cat /user/cloudera/wordcount/output/* | awk '{print $2}' | sort | tr '\n' ',' | \
	awk '{print $1"\n"}' > stopwords.csv

	hadoop fs -put stopwords.csv /user/cloudera/wordcount/

delete:
	hadoop fs -rm -r -f /user/cloudera/wordcount/output /user/cloudera/temp_out


REPORT:





#######################################################################################################

ex2b


