package org.myorg;

import java.nio.ByteBuffer;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;
import java.io.IOException;
import java.util.regex.Pattern;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.StringUtils;
import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.io.compress.CompressionCodec;

import org.apache.log4j.Logger;

public class WordCount extends Configured implements Tool {
  
  private static final Logger LOG = Logger.getLogger(WordCount.class);
  
  public static void main(String[] args) throws Exception {
    int res = ToolRunner.run(new WordCount(), args);
    System.exit(res);
  }

  public int run(String[] args) throws Exception {
    Job job = Job.getInstance(getConf(), "wordcount");
    job.setJarByClass(this.getClass());
    Path myout = new Path("/user/cloudera/wordcount/");
    
    // Use TextInputFormat, the default unless job.setInputFormatClass is used
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(myout, "temp_out"));
    job.setMapperClass(Map.class);
    job.setCombinerClass(Reduce.class);
    job.setReducerClass(Reduce.class);
    /*
    job.getConfiguration().setBoolean("mapreduce.compress.map.output", true);
    job.getConfiguration().setClass("mapreduce.map.output.compression.codec", SnappyCodec.class, CompressionCodec.class);
    job.getConfiguration().setBoolean("mapreduce.map.output.compress", true);
    job.getConfiguration().setBoolean("mapreduce.output.fileoutputformat.compress", false);
    
	*/


    job.getConfiguration().set("mapred.compress.map.output", "true");
    job.getConfiguration().set("mapred.output.compression.type", "BLOCK"); 
    job.getConfiguration().set("mapred.map.output.compression.codec", "org.apache.hadoop.io.compress.GzipCodec"); 
	
    
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(IntWritable.class);
    job.setNumReduceTasks(50);
	
    while (!job.waitForCompletion(true))
	{
    	System.out.println("Waiting....");
	}	
    
    Job job2 = Job.getInstance(getConf(), "sort_by_freq");
    
    job2.setJarByClass(this.getClass());
    // Use TextInputFormat, the default unless job.setInputFormatClass is used
    FileInputFormat.addInputPath(job2, new Path(myout, "temp_out"));
    FileOutputFormat.setOutputPath(job2, new Path(args[1]));
    job2.setMapperClass(Map2.class);
    
    
    job2.setReducerClass(Reduce2.class);
    
    // SWAP!
    job2.setOutputKeyClass(IntWritable.class);
    
    job2.setOutputValueClass(Text.class);
    job2.setSortComparatorClass(ReverseComparator.class);
    
    while (!job2.waitForCompletion(true))
   	{
       	System.out.println("Waiting....");
   	}
   	
    System.out.println("Done!");
    return 0;
    

  }

  public static class Map extends Mapper<LongWritable, Text, Text, IntWritable> {
    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();
    private boolean caseSensitive = false;
    private long numRecords = 0;
    private String input;
    private static final Pattern WORD_BOUNDARY = Pattern.compile("\\s*\\b\\s*");
    //private static final Pattern REGEX = Pattern.compile("[a-zA-Z]+(?:'[a-zA-Z]+)?");

    protected void setup(Mapper.Context context)
      throws IOException,
        InterruptedException {
    
	  if (context.getInputSplit() instanceof FileSplit) {
		    this.input = ((FileSplit) context.getInputSplit()).getPath().toString();
	  } else {
		    this.input = context.getInputSplit().toString();
	  }	
      Configuration config = context.getConfiguration();
      this.caseSensitive = config.getBoolean("wordcount.case.sensitive", false);
    }
    
    public void map(LongWritable offset, Text lineText, Context context)
		throws IOException, InterruptedException {
			String line = lineText.toString();
			if (!caseSensitive) {
				line = line.toLowerCase();
			}
			Text currentWord = new Text();
			for (String word : WORD_BOUNDARY.split(line)) {
				if (word.isEmpty()) {
					continue;
				} else if (word.matches("[a-zA-Z]+(?:'[a-zA-Z]+)?")){
					currentWord = new Text(word);
					context.write(currentWord,one);
				}
			}         
		}
  }
  
  public static class Map2 extends Mapper< LongWritable, Text,  IntWritable, Text> {
	   
	    public void map(LongWritable offset, Text lineText, Context context) throws IOException, InterruptedException {
	    	String line = lineText.toString();
	    	String arr[] = line.split("\t", 2);
	    	if (Integer.parseInt(arr[1]) > 4000)
	    	    context.write(new IntWritable(Integer.parseInt(arr[1])), new Text(arr[0]));
	      }
	  }
  
  public static class Reduce extends Reducer<Text, IntWritable, Text, IntWritable> {
    @Override
    public void reduce(Text word, Iterable<IntWritable> counts, Context context)
        throws IOException, InterruptedException {
      int sum = 0;
      for (IntWritable count : counts) {
        sum += count.get();
      }
      context.write(word, new IntWritable(sum));
    }
  }



public static class Reduce2 extends Reducer<IntWritable, Text, IntWritable, Text> {
    @Override
    public void reduce(IntWritable freq, Iterable<Text> words, Context context)
        throws IOException, InterruptedException {
	      //Text sum = new Text();
	      for (Text word : words) {
	    	  context.write(freq, word);
	      }
      }
  }

	
	public static class ReverseComparator extends WritableComparator {
	     
	    private static final Text.Comparator TEXT_COMPARATOR = new Text.Comparator();
	    public ReverseComparator() {
	        super(Text.class);
	    }
	 
	    @Override
	    public int compare(byte[] b1, int s1, int l1, byte[] b2, int s2, int l2) {
	       return (-1)* TEXT_COMPARATOR.compare(b1, s1, l1, b2, s2, l2);
	    }
	 
	    @SuppressWarnings("rawtypes")
	    @Override
	    public int compare(WritableComparable a, WritableComparable b) {
	        if (a instanceof Text && b instanceof Text) {
	                return (-1)*(((Text) a).compareTo((Text) b));
	        }
	        return super.compare(a, b);
	    }
	}
}
