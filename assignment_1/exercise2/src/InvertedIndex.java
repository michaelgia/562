package org.myorg;

import java.nio.ByteBuffer;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;
import java.io.IOException;
import java.util.regex.Pattern;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.StringUtils;

import org.apache.log4j.Logger;

public class InvertedIndex extends Configured implements Tool {

	private static final Logger LOG = Logger.getLogger(InvertedIndex.class);

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new InvertedIndex(), args);
		System.exit(res);
	}

	public int run(String[] args) throws Exception {
		Job job = Job.getInstance(getConf(), "invertedindex");
		for (int i = 0; i < args.length; i += 1) {
			if ("-skip".equals(args[i])) {
				job.getConfiguration().setBoolean("invertedindex.skip.patterns", true);
				i += 1;
				job.addCacheFile(new Path(args[i]).toUri());
				// this demonstrates logging
				LOG.info("Added file to the distributed cache: " + args[i]);
			}
		}

		job.setJarByClass(this.getClass());
		Path myout = new Path("/user/cloudera/invertedindex/");

		// Use TextInputFormat, the default unless job.setInputFormatClass is used
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(myout, "temp_out"));
		job.setMapperClass(Map.class);
		//job.setCombinerClass(Reduce.class);
		job.setReducerClass(Reduce.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		//job.setNumReduceTasks(10);

		//return job.waitForCompletion(true) ? 0 : 1;
		
		while (!job.waitForCompletion(true))
		{
			System.out.println("Waiting....");
		}	
		
		Job job2 = Job.getInstance(getConf(), "single_instanced_words");
		job2.setJarByClass(this.getClass());
		// Use TextInputFormat, the default unless job.setInputFormatClass is used
		FileInputFormat.addInputPath(job2, new Path(myout, "temp_out"));
		FileOutputFormat.setOutputPath(job2, new Path(args[1]));
		job2.setMapperClass(Map2.class);
		job2.setReducerClass(Reduce2.class);
		job2.setOutputKeyClass(IntWritable.class);
		job2.setOutputValueClass(Text.class);
		
		while (!job2.waitForCompletion(true))
		{
			System.out.println("Waiting....");
		}
		
		System.out.println("Done!");
		return 0;
		
		//return job.waitForCompletion(true) ? 0 : 1;

	}

	public static class Map extends Mapper<LongWritable, Text, Text, Text> {
		private final static IntWritable one = new IntWritable(1);
		private Text word = new Text();
		private boolean caseSensitive = false;
		private long numRecords = 0;
		private String input;
		private Set<String> patternsToSkip = new HashSet<String>();
		private static final Pattern WORD_BOUNDARY = Pattern.compile("\\s*\\b\\s*");
		private static final Pattern CSV_BOUNDARY = Pattern.compile(",");
				
		private static final Pattern REGEX = Pattern.compile("[a-zA-Z]+(?:'[a-zA-Z]+)?");

		protected void setup(Mapper.Context context)
			throws IOException,
			       InterruptedException {

				       if (context.getInputSplit() instanceof FileSplit) {
					       this.input = ((FileSplit) context.getInputSplit()).getPath().toString();
				       } else {
					       this.input = context.getInputSplit().toString();
				       }	
				       Configuration config = context.getConfiguration();
				       this.caseSensitive = config.getBoolean("invertedindex.case.sensitive", false);
				       if (config.getBoolean("invertedindex.skip.patterns", false)) {
					       URI[] localPaths = context.getCacheFiles();
					       parseSkipFile(localPaths[0]);
				       }
			       }

		private void parseSkipFile(URI patternsURI) {
			LOG.info("Added file to the distributed cache: " + patternsURI);
			try {
				BufferedReader fis = new BufferedReader(new FileReader(new File(patternsURI.getPath()).getName()));
				String line;
				while ((line = fis.readLine()) != null) {
					//patternsToSkip.add(pattern);

					for (String word : CSV_BOUNDARY.split(line)) {
						if (word.isEmpty()) {
							continue;
						} 
						patternsToSkip.add(word);
					} 
				}
			} catch (IOException ioe) {
				System.err.println("Caught exception while parsing the cached file '"
						+ patternsURI + "' : " + StringUtils.stringifyException(ioe));
			}
		}

		public void map(LongWritable offset, Text lineText, Context context)
			throws IOException, InterruptedException {
				String line = lineText.toString();
				String filename = ((FileSplit) context.getInputSplit()).getPath().getName();
				if (!caseSensitive) {
					line = line.toLowerCase();
				}
				Text currentWord = new Text();
				for (String word : WORD_BOUNDARY.split(line)) {
					//char c = word.getSymbol().charAt(0);
					if (word.isEmpty() || patternsToSkip.contains(word)) {
						continue;
					} else if (word.matches("[a-zA-Z]+(?:'[a-zA-Z]+)?")){
										
						// one more step to check if alpharithmetic
						
						currentWord = new Text(word);
						context.write(currentWord,new Text(filename));
					}
				}         
			}
	}

	public static class Map2 extends Mapper< LongWritable, Text, IntWritable, Text> {
		int singleinstcounter = 1;
		public void map(LongWritable offset, Text lineText, Context context) throws IOException, InterruptedException {
			String line = lineText.toString();
			String arr[] = line.split("\t", 3);
			System.out.println(arr[0]+"+"+arr[1]+"+"+arr[2]+"+"+arr[2].split("").length);
			int count = 1;			
			for (int i = 0; i < arr[2].length(); i++)
			{
				if (arr[2].charAt(i) == ' ')
				{
			    		count++;
				}
			}
			if (count == 1)
			{
				context.write(new IntWritable(singleinstcounter), new Text(arr[1]));
				singleinstcounter++;
			}
		}
	}

	public static class Reduce extends Reducer<Text, Text, IntWritable, Text> {
			
			int wordscounter = 1;
			
			@Override			
			public void reduce(Text word, Iterable<Text> documents, Context context)
			throws IOException, InterruptedException {
				String docs = "";
				boolean firstentry = true;
				for (Text doc : documents)
				{
						if (firstentry)
						{
							firstentry = false;
							docs += doc;
						}
						else if (!docs.contains(doc.toString()))
						{
							docs += ", " + doc;
						}

				}
				context.write(new IntWritable(wordscounter), new Text(word+"\t"+docs));
				wordscounter++;
			}
	}



	public static class Reduce2 extends Reducer<IntWritable, Text, IntWritable, Text> {
		int singleinstcounter = 1;
		//@Override
			public void reduce(IntWritable num, Text word, Context context)
			throws IOException, InterruptedException {
				context.write(new IntWritable(2), word);
				singleinstcounter++;
				
			}
	}
}
