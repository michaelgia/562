Compile & run exercise 1:

hadoop fs -mkdir /user/cloudera/wordcount /user/cloudera/wordcount/input
hadoop fs -put input/* /user/cloudera/wordcount/input
javac -cp /usr/lib/hadoop/*:/usr/lib/hadoop-mapreduce/* src/WordCount.java -d build -Xlint 
jar -cvf wordcount.jar -C build/ . 
hadoop fs -rm -r -f /user/cloudera/wordcount/temp_out /user/cloudera/wordcount/output 
hadoop jar wordcount.jar org.myorg.WordCount /user/cloudera/wordcount/input /user/cloudera/wordcount/output

Retrieve data from output:
hadoop fs cat /user/cloudera/wordcount/output/* | awk '{print $2"\t"$1}' | head -10 > first10.txt
hadoop fs cat /user/cloudera/wordcount/output/* | awk '{print $2}' | sort | tr '\n' ',' | awk '{print $1"\n"}' > stopwords.csv

Compile & run exercise 2b or 3:

hadoop fs -mkdir /user/cloudera/invertedindex /user/cloudera/invertedindex/input
hadoop fs -put input/* /user/cloudera/invertedindex/input
hadoop fs -put stopwords.csv /user/cloudera/invertedindex/
javac -cp /usr/lib/hadoop/*:/usr/lib/hadoop-mapreduce/* src/InvertedIndex.java -d build -Xlint 
jar -cvf invertedindex.jar -C build/ . 
hadoop fs -rm -r -f /user/cloudera/invertedindex/temp_out /user/cloudera/invertedindex/output 
hadoop jar invertedindex.jar org.myorg.InvertedIndex /user/cloudera/invertedindex/input /user/cloudera/invertedindex/output -skip /user/cloudera/invertedindex/stopwords.csv


